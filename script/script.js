let changeThemeButtons = document.querySelectorAll('.change-theme');

changeThemeButtons.forEach(button => {
    button.addEventListener('click', function () {
        let theme = this.dataset.theme;
        applyTheme(theme);
        localStorage.setItem('theme', theme);
    });
});

function applyTheme(themeName) {
    let themeUrl = `style/theme-${themeName}.css`
    document.querySelector('[title = "theme"]').setAttribute('href', themeUrl)
}

let activeTheme = localStorage.getItem('theme');
if (activeTheme == null){
    applyTheme('light')
} else {
    applyTheme(activeTheme)
}